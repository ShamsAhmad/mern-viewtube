import React, { useState } from "react";
import styled from "styled-components";
import Card from "../components/Card";
// import Menu  from "./Menu";
import Navbar from "./Navbar";
import SideBar  from "./sidebar";
import "../components/sidebar.css"
import { useEffect } from "react";
import axios from "axios"
import "./landing.css"
const Container = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const  Landing =() => {
  // const[search,setSearch]=useState("");
  const [videos,setVideo]=useState([]);
  useEffect(()=>{
    async function data(){
      try {
      
        let response = await axios.get('http://localhost:5000/mostpopular');
      console.log(response)
      setVideo(response.data)
      }
       catch (error) {
        console.error(error.response.data);     // NOTE - use "error.response.data` (not "error")
      }

    }
    data();
 },[]);
  
  async function callback(search){
    // setSearch(search);
    // console.log(search)
    //console.log(video.data);
    let response = await axios.get('http://localhost:5000/search',
    { 
      params:{search_query:search} }
    );
    console.log(response.data);
    setVideo(response.data)
  }

  return (
    <>
    <Navbar onClick={callback}/>
    <div className="landing">
      <SideBar />
  
      <div className="video-grid">
      {
        videos.map(video => {
          return (
          <Card videoId={video.id.videoId?video.id.videoId:video.id} /> 
          )
        })
        
      }
      </div>
  </div>
    </>
  );
};

export default Landing;
