import React from "react";
import styled from "styled-components";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import { Link } from "react-router-dom";
import { useState } from "react";
import axios from "axios";
import Dropdown from 'react-bootstrap/Dropdown';
import  "../components/navbar.css"
import viewtubeImg from "../assets/viewtube.png"

const Container = styled.div`
  position: sticky;
  top: 0;
  background-color: ${({ theme }) => theme.bgLighter};
  height: 56px;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  height: 100%;
  padding: 0px 20px;
  position: relative;
`;

const Search = styled.div`
  width: 40%;
  position: absolute;
  left: 0px;
  right: 0px;
  margin: auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 5px;
  border: 1px solid #ccc;
  border-radius: 3px;
`;

const Input = styled.input`
  border: none;
  background-color: transparent;
  outline: none;
  color: ${({ theme }) => theme.text};
`;

const Button = styled.button`
  padding: 5px 15px;
  background-color: transparent;
  border: 1px solid #3ea6ff;
  color: #3ea6ff;
  border-radius: 3px;
  font-weight: 500;
  cursor: pointer;
  display: flex;
  align-items: center;
  gap: 5px;
`;
const Navbar = (props) => {
  const [search, setSearch] = useState("");
  function handleChange(e) {
    const { value, name } = e.target;
    setSearch(prev => {
      return { ...prev, [name]: value }
    })

  }
  async function handleClick(e) {
    console.log(search);
    // let data = await axios.get('http://localhost:5000/search',
    // { 
    //   params:{search_query:search} }
    // );
    props.onClick(search);
    // console.log(data);
    
  }
  return (
    <Container className="landing-nav">
      
      <Wrapper>
      <img style={{height:'35px',width:"35px"}}src={viewtubeImg}/>
        <Search>
          <Input placeholder="Search" style={{color:"white"}}  onChange={handleChange} />
          <SearchOutlinedIcon onClick={handleClick} className="searchIcon"/>
        </Search>






        <Dropdown className="dropMenu">
         
          <Dropdown.Toggle variant="grey" id="dropdown-basic" className="dropMenu">
          <AccountCircleOutlinedIcon />
            Profile
          </Dropdown.Toggle>

          <Dropdown.Menu >
          <Dropdown.Item > <Link to="/GetProfile" style={{ textDecoration: "none" }}>Profile </Link> </Dropdown.Item>
            <Dropdown.Item > <Link to="/UpdateProfile" style={{ textDecoration: "none" }}>Edit Profile </Link> </Dropdown.Item>
           
            <Dropdown.Item > <Link to="/ChangePassword" style={{ textDecoration: "none" }}> Change Password </Link></Dropdown.Item>
            
          </Dropdown.Menu>
        </Dropdown>


      </Wrapper>
    </Container>
  );
};

export default Navbar;
