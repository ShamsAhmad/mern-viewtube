import React from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { Formik, Form } from 'formik';
import { TextField } from './TextField';
import * as Yup from 'yup';
import viewtubeImg from "../assets/viewtube.png";
import changePass from "../assets/changePass.png";
import axios  from 'axios';
import  config  from '../config';

 const ForgetPassword = ()=>{
    
    const validate = Yup.object({
        email: Yup.string()
        .email('Email is invalid')
        .required('Email is required'),
    })




    return(
        <>
  <Formik
      initialValues={{
        oldPassword: '',
        newPassword:'',
        confirmNewPassword:''
      }}
      validationSchema={validate}
      onSubmit={ async (values,{resetForm}) => {
       try{
        let token = document.cookie.split('=')[1];
        await axios.post(config.changePasswordURL,values,{
          headers: {
            "Content-Type": "application/json",
            "Authorization":token
        }, withCredentials:true
        });
        alert("password change successfully");
        resetForm();
        
       }
       catch{
        alert('Please Check your Current Passsword');
       }
        
    
      }}
    >
      {formik => (
        <>
        <div className="container-fluid">
        <div className="row">
        <Navbar  bg="dark" variant="dark">
        <Container>
          <Navbar.Brand style={{margin:"auto"}} >View <img style={{height:'35px',width:"35px"}}src={viewtubeImg}/> Tube</Navbar.Brand>
         
        </Container>
      </Navbar>
          <div  className="col-md-5 mt-6" style={{marginTop:"90px"}}>
        <div>
          <h1 style={{margin: "30px 0px 0px 25px"}} className="my-1  font-weight-bold .display-1">Forget Password</h1>
          <Form style={{margin: "30px 0px 0px 25px"}} onSubmit={formik.handleSubmit}>
         
          <TextField 
            label="Email"
             name="email"
             placeholder="xyz@gmail.com"
              type="email" 
              value={formik.values.email}
              onChange={formik.handleChange}
              />
          
            <button className="btn btn-dark mt-3" type="submit">send</button>
          </Form>
        </div>
        </div>
        <div className="col-md-7 my-auto">
        <img style={{width:"500px",minWidth:"200px" ,marginLeft: '5%'}}className="img-fluid w-70" src={changePass} alt=""/>
        </div>
      </div>
    </div>
    <div style={{width:"100%", textAlign:"center",height:"40px", marginTop:"39px",backgroundColor:"darkgrey"}}><h5 style={{color:"white",paddingTop:"6px"}}>ViewTube &copy; 2022</h5></div>
    </>
      )}
    </Formik>
        </>
    )
}
export default ForgetPassword;