//eslint-disable-next-line 
import './App.css';
import { Signup } from './components/Signup';
import {Signin} from './components/Signin';
import {BrowserRouter as Router,Routes,Route} from "react-router-dom";
import {Home} from "./components/Home";
import rocketImg from './assets/sign.png';
import  Landing  from './components/landing';
import Menu from "./components/Menu";
import Navbar from './components/Navbar';
import ChangePassword from './components/ChangePassword';
import UpdateProfile from './components/UpdateProfile';
import GetProfile from './components/GetProfile';
import Test from "./components/Test"
import ForgetPassword from './components/ForgetPassword';
// const Container = styled.div`
//   display: flex;
// `;

// const Main = styled.div`
//   flex: 7;
//   background-color: ${({ theme }) => theme.bg};
// `;
// const Wrapper = styled.div`
//   padding: 22px 96px;
// `;

function App() {
  return (
    
      <Router>
      <Routes>
        <Route exact path="/" element={<Home/>} />
          <Route exact path="/Signup" element={<Signup/>} />
          <Route exact path="/Signin" element={<Signin/>} />
          <Route exact path="/Landing" element={<Landing/>} />
          <Route exact path="/ChangePassword" element={<ChangePassword/>} />
          <Route exact path="/UpdateProfile" element={<UpdateProfile/>} />
          <Route exact path="/GetProfile" element={<GetProfile/>} />
          <Route path="/video/:id" element={<Test />} />
          <Route exact path="/ForgetPassword" element={<ForgetPassword/>} />
        </Routes>
       
        
    </Router>
    
  );
}

export default App;
