const express=require("express");
const axios = require('axios');
const app= express();
const port = 5000;
const apiKey = "AIzaSyAp85aRKyHND2w51qRTzZN_Fr4jp1KALEc";
const baseApiUrl = "https://www.googleapis.com/youtube/v3";
const cors=require('cors');

app.use(cors());

app.get("/" ,(req,res)=>{
    res.send("hello from our api");
});

app.get('/search',async(req,res,next)=>{
    
    try{
    
    const searchQuery = req.query.search_query;
    console.log(searchQuery);
    const url= `${baseApiUrl}/search?key=${apiKey}&type=video&part=snippet&maxResults=30&q=${searchQuery}`;
    const response=await axios.get(url);
    //const titles = response.data.items.map((item) => item.snippet.title);
    res.send(response.data.items);
    } catch(err){
        next(err);
    }
});



app.get('/mostpopular',async(req,res,next)=>{

    try{
        const url=`${baseApiUrl}/videos?part=snippet&chart=mostPopular&regionCode=in&maxResults=30&key=${apiKey}`;
        const response=await axios.get(url);
        // const titles= response.data.items.map((item)=>item.snippet.title);
        // //res.send(response.data.items);
        res.send(response.data.items);
    }
    catch(err){
        next(err);
    }
});

app.get('/category',async(req,res,next)=>{
    try{
        const videoId=req.query.videoCategoryId;
        const url=`${baseApiUrl}/videos?part=snippet&regionCode=in&maxResults=10&videoCategoryId=${videoId}&chart=mostPopular&key=${apiKey}`;
        const response=await axios.get(url);
        //const titles= response.data.items.map((item)=>item.snippet.title);
        res.send(response.data.items);
        //res.send(titles);
    }
    catch(err){
        next(err);
    }
});



app.listen(port ,()=>{
    console.log("server is running on port 5000");
});
